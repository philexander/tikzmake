% \iffalse meta-comment
% Author: Christoph Reller, christoph.reller@gmail.com
% \fi
%
% \iffalse meta-comment <general public licence>
%%
%% tikzmake package
%% Copyright (c) 2013 Christoph Reller
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 2 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
%%
% \fi
%
% \iffalse
%<*driver>
\ProvidesFile{tikzmake.dtx}
%</driver>
%<package>\NeedsTeXFormat{LaTeX2e}[1999/12/01]
%<package>\ProvidesPackage{tikzmake}
%<*package|driver>
    [2012/12/19 v0.2]
%</package|driver>
%<package>\RequirePackage{tikz}
%<package>\RequirePackage{pgfplots}
%<package>\RequirePackage{ifpdf}
%<package>\RequirePackage{kvoptions}
%<*driver>
\documentclass{ltxdoc}
\usepackage{ltxdocext}
\usepackage{tikzmake}
\usepackage{hypdoc}
\usepackage{tabularx}
\AtBeginDocument{\CodelineIndex\EnableCrossrefs\RecordChanges}
\begin{document}
\DocInput{tikzmake.dtx}
\end{document}
%</driver>
%<*package>
% \fi
%
% \CheckSum{63}
%
% {\makeatother
% \CharacterTable
% {Upper-case      \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%  Lower-case      \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%  Digits          \0\1\2\3\4\5\6\7\8\9
%  Exclamation     \!      Double quote \"     Hash (number) \#
%  Dollar          \$      Percent      \%     Ampersand     \&
%  Acute accent    \'      Left paren   \(     Right paren   \)
%  Asterisk        \*      Plus         \+     Comma         \,
%  Minus           \-      Point        \.     Solidus       \/
%  Colon           \:      Semicolon    \;     Less than     \<
%  Equals          \=      Greater than \>     Question mark \?
%  Commercial at   \@      Left bracket \[     Backslash     \\
%  Right bracket   \]      Circumflex   \^     Underscore    \_
%  Grave accent    \`      Left brace   \{     Vertical bar  \|
%  Right brace     \}      Tilde        \~}
% }
%
% \changes{v0.2}{2012/12/19}{Initial version}
%
% \GetFileInfo{tikzmake.sty}
%
% \DoNotIndex{\#,\$,\%,\&,\@,\^,\_,\,,\~,\!,\ ,\\,\,\{,\}}
%
% \DoNotIndex{\DeclareStringOption, \else, \fi, \ifpdf, \image,
%   \input, \pgfplotstableread, \plotdata,
%   \ProcessKeyvalOptions, \providecommand, \relax, \texsource,
%   \tikzexternalcheckshellescape, \tikzexternalize, \tikzpicturedependsonfile,
%   \tikzset, \tikzsetnextfilename, \usetikzlibrary}
%
% \title{The \textsf{tikzmake} package\thanks{This document corresponds to
%   \textsf{tikzmake}~\fileversion, dated \filedate.}}
%
% \author{Christoph Reller\\ \texttt{creller@ee.ethz.ch} \\}
%
% \maketitle
%
% \begin{abstract}
%   Make use of the \texttt{list and make} mode of the Ti\emph{k}Z
%   externalization.
% \end{abstract}
%
% \section{Introduction}
%
% The Ti\emph{k}Z externalization library (see \texttt{pgfmanual.pdf}) provides
% automatic “pre-compilation” of |tikzpicture| environments.  When the \LaTeX\
% document is compiled, the externalization library replaces a |tikzpicture|
% environment by an |\includegraphics| command that inserts the pre-compiled
% picture provided it exists and otherwise re-compiles it.  This can result in
% much quicker compilation when doing small changes to the \LaTeX\ document.
% 
% There is, however, a problem with detecting when a |tikzpicture| environment
% has changed and needs to be rebuilt.  The idea is to use a separate source
% file for each Ti\emph{k}Z picture and to use the \texttt{list and make} mode
% of the Ti\emph{k}Z externalization library.  In this mode, Ti\emph{k}Z
% produces a makefile describing dependencies that have been declared with
% \verb|\tikzpicturedependsonfile|.  The purpose of the |tikzmake| package is to
% automatically make use of this makefile and to simplify the externalization in
% \texttt{list and make} mode.
%
% \section{Usage}
% The |tikzmake| package consists of a makefile and a \LaTeX{} package, both of
% which cooperate, but can be used independently.
%
% \subsection{Automatic Build with a GNU Makefile}\label{sec:makefile}
% The provided GNU makefile (|tikzmake|) is able to compile ``any'' \LaTeX{}
% document\footnote{Note that there may be more elaborate and general makefiles
% for \LaTeX\ around, e.g. ``The'' \LaTeX\ Makefile
% \url{http://code.google.com/p/latex-makefile/}}.  The procedure to use it is
% simple:
% \begin{enumerate}
% \item Copy |tikzmake| into the directory where the main \LaTeX{} source
% file is located and rename |tikzmake| to |Makefile|.
% \item In a shell, navigate to this directory and type |make|.
% \end{enumerate}
% The makefile has some basic support for:
% \begin{itemize}
% \item multiple \LaTeX\ source files (no recursive checks for inclusions)
% \item detecting if |pdflatex| or |latex| should be used
% \item running |bibtex| for bibliography generation if needed
% \item running |makeindex| for index generation and nomenclatures if needed
% \item detecting additional needed |(pdf)latex| runs for missing labels and
%   missing citations
% \item the Ti\emph{k}Z externalization library in \texttt{list and make} mode
% \end{itemize}
% The makefile defines the following targets:
% \begin{description}
% \item[|all|:] builds everything, deletes nothing
% \item[\texttt{clean\_intermediate}:] deletes all output files except the final
%   pdf file(s) and all pre-compiled Ti\emph{k}Z picture environments.
% \item[\texttt{clean\_dep}:] deletes all additionally generated makefiles
% \item[|clean|:] deletes all output files except the resulting pdf file(s)
% \item[\texttt{clean\_all}:] deletes all output files
% \end{description}
%
% \subsection{Easy Externalization}\label{sec:external}
% \subsubsection{Preliminaries}
% In the following I explain by example, how to use the externalization
% mechanism.  Note that various directory and file names can be customized via
% options (see Section~\ref{sec:options}).  Also note that for the
% externalization to work you have to call \texttt{(pdf)latex} with the option
% \texttt{-shell-escape}.
%
% Let us assume that you are working on a document that contains |tikzpicture|
% environments, some of which you would like to pre-compile, i.e., externalize.
% All pre-compiled pdf files will reside in a subdirectory |tikzext|.  You have
% to create this directory, unless you are using the |tikzmake| makefile.  You
% load the package in the preamble with:
%
% \medskip\noindent |\usepackage{tikzmake}| \medskip
% 
% \noindent This in itself has no effect, since the |tikzmake| package does not
% enable pre-compilation per default for all pictures.
%
% \subsubsection{Pictures}\label{sec:picts}
% \DescribeMacro{\inputtikz} Assume that in the text body, you have:
%
% \medskip\noindent \meta{before}\\
% |\begin{tikzpicture}|\\
% \indent\meta{tikzpicture-contents}\\
% |\end{tikzpicture}|\\
% \meta{after} \medskip
%
% \noindent To enable externalization for this |tikzpicture|, you substitute the
% above by:
%
% \medskip\noindent \meta{before}\\
% \cmd{\inputtikz}|{|\meta{pict-filename}|}|\\
% \meta{after} \medskip
%
% \noindent and in the subdirectory |picts| (create this directory if it does
% not yet exist) you create a new file \meta{pict-filename}\texttt{.tex} that
% contains:
%
% \medskip\noindent |\begin{tikzpicture}|\\
% \indent\meta{tikzpicture-contents}\\
% |\end{tikzpicture}| \medskip
%
% In principle, the above works for any |tikzpicture| environment, also for
% plots done with |pgfplots|.  For the case where plots depend on an external
% data file, we provide an additional macro.
%
% \subsubsection{Plots}\label{sec:plots}
% \DescribeMacro{\inputplot} Assume that in the text body, you have the
% following plot:
%
% \medskip\noindent \meta{before}\\
% |\pgfplotstableread{../plotdat/|\meta{plot-filename}|.dat}\plotdata|\\
% |\begin{tikzpicture}|\\
% \indent\meta{plot-contents}\\
% |\end{tikzpicture}|\\
% \meta{after} \medskip
% 
% \noindent Note that the above uses some datafile in a subdirectory
% |plotdat| and loads this data into a macro |\plotdata|.\footnote{See
% \texttt{pgfplots.pdf} for how to construct plots in this mode.} To enable
% externalization for this plot, you substitute the above by:
%
% \medskip\noindent \meta{before}\\
% \cmd{\inputplot}|{|\meta{plot-filename}|}|\\
% \meta{after} \medskip
%
% \noindent and in the subdirectory |plots| (create this directory if it does
% not yet exist) you create a new file \meta{plot-filename}\texttt{.tex} that
% contains:
%
% \medskip\noindent |\begin{tikzpicture}|\\
% \indent\meta{plot-contents}\\
% |\end{tikzpicture}| \medskip
%
% \subsubsection{Work Cycle Without \texttt{tikzmake}
% Makefile}\label{sec:workcycle}
% With the externalization functionality provided by \cmd{\inputtikz} and
% \cmd{\inputplot}, pre-compiled Ti\emph{k}Z pictures are not updated
% automatically when the source file has changed.  In \texttt{list and make}
% mode, the Ti\emph{k}Z externalization library produces, however, a makefile
% \meta{maintex}|.makefile|, where \meta{maintex} is the name of the main
% \LaTeX{} file without the \texttt{.tex} extension.  With the help of this
% makefile, typical work cycle scenarios are:
%
% \paragraph{Externalize an existing Ti\emph{k}Z picture:}
% Proceed as in Section~\ref{sec:picts} or~\ref{sec:plots} and \LaTeX{} as:\\
% \indent|pdflatex -shell-escape| \meta{maintex}|.tex|\\
% or:\\
% \indent|latex -shell-escape| \meta{maintex}|.tex|
%
% \paragraph{Change an externalized Ti\emph{k}Z picture:}
% Use the makefile \meta{maintex}|.makefile| to recompile the changed source
% files:\\
% \indent|make -f| \meta{maintex}|.makefile|
%
% \paragraph{Remove an externalized Ti\emph{k}Z picture:}
% Remove the source file \meta{filename}\texttt{.tex} and the pre-compilation
% output, i.e., all the files \texttt{tikzext/}\meta{filename}\texttt{.*}.  Make
% a normal |(pdf)latex| run.
%
% \subsection{Externalization and Makefile}
% The |tikzmake| makefile as described in Section~\ref{sec:makefile} and the
% |tikzmake| \LaTeX{} package as described in Section~\ref{sec:external} work
% hand in hand.  Specifically, all the work cycle scenarios in
% Section~\ref{sec:workcycle} are substituted by typing |make| on the shell.
% This includes the tracking of changes in externalized Ti\emph{k}Z pictures and
% automatic creation of directories.
%
% \section{Package Options}\label{sec:options}
% The package uses the \meta{key}=\meta{value} paradigm for options.  All
% options are string options.
%
% \noindent\begin{tabularx}{\linewidth}{@{}llX@{}}
%   \meta{key} & \textit{default} & \textit{meaning} \\
%   |extdir| & |tikzext| & Directory containing the pre-compiled output files
%   for Ti\emph{k}Z pictures externalized both with
%   \cmd{\inputtikz} and \cmd{\inputplot}.\\
%   |pictdir| & |picts| & Directory containing the source files for Ti\emph{k}Z
%   picture environments externalized with
%   \cmd{\inputtikz}.\\
%   |pictprefix| & & Prefix prepended to filenames given to
%   \cmd{\inputtikz} to construct the final \LaTeX{} source filename.\\
%   |plotdir| & |plots| & Directory containing the source
%   files for plots externalized with \cmd{\inputplot}.\\
%   |plotprefix| & & Prefix prepended to filenames given to
%   \cmd{\inputplot} to construct the final \LaTeX{} source filename.\\
%   |datadir| & |plotdat| & Directory containing the data
%   files for plots externalized with \cmd{\inputplot}.\\
%   |dataprefix| & & Prefix prepended to filenames given to \cmd{\inputplot} to
%   construct the final filename (without filename
%   extension) of data files.\\
%   |dataext| & |dat| & Filename extension appended to filenames given to
%   \cmd{\inputplot} to construct the final filename of data files.
% \end{tabularx}\medskip
% 
% \subsubsection*{Summary}
% \noindent|\inputtikz{|\meta{name}|}| implies the following file names:
%
% \medskip\noindent\begin{tabular}{@{}lll@{}}
%   & & \textit{default:} \\
%   Source file &
%   \meta{pictdir}|/|\meta{pictprefix}\meta{name}|.tex| &
%   |picts/|\meta{name}|.tex| \\
%   Output file &
%   \meta{extdir}|/|\meta{pictprefix}\meta{name}|.pdf| &
%   |tikzext/|\meta{name}|.pdf|
% \end{tabular}\bigskip
%
% \noindent|\inputplot{|\meta{name}|}| implies the following file names:
%
% \medskip\noindent\begin{tabular}{@{}lll@{}}
%   & & \textit{default:}\\
%   Source file &
%   \meta{plotdir}|/|\meta{plotprefix}\meta{name}|.tex| &
%   |plots/|\meta{name}|.tex| \\
%   Data file &
%   \meta{datadir}|/|\meta{dataprefix}\meta{name}|.|\meta{dataext} &
%   |plotdat/|\meta{name}|.dat| \\
%   Output file &
%   \meta{extdir}|/|\meta{plotprefix}\meta{name}|.pdf| &
%   |tikzext/|\meta{name}|.pdf|
% \end{tabular}
% 
% \StopEventually{
% \PrintChanges
% 
% \pdfbookmark{Index}{indexpdfbookmark}\phantomsection%
% \PrintIndex}
%
% \section{Implementation}
% Declare the options:
%    \begin{macrocode}
\DeclareStringOption[tikzext]{extdir}
\DeclareStringOption[picts]{pictdir}
\DeclareStringOption[]{pictprefix}
\DeclareStringOption[plots]{plotdir}
\DeclareStringOption[]{plotprefix}
\DeclareStringOption[plotdat]{datadir}
\DeclareStringOption[]{dataprefix}
\DeclareStringOption[dat]{dataext}
\ProcessKeyvalOptions*\relax
%    \end{macrocode}
% Load the Ti\emph{k}Z externalization library, choose the |list and make| mode
% and disable externalization per default:
%    \begin{macrocode}
\usetikzlibrary{external} 
\tikzexternalize[prefix=\tikzmake@extdir/,mode=list and make,export=false]
%    \end{macrocode}
% Use different external system calls in pdf and dvi mode as suggested in the
% \texttt{pgfmanual.pdf}:
%    \begin{macrocode}
\ifpdf
\tikzset{external/system call={pdflatex \tikzexternalcheckshellescape
    -halt-on-error -interaction=batchmode -jobname '\image' '\texsource'}}
\else
\tikzset{external/system call={latex \tikzexternalcheckshellescape
    -halt-on-error -interaction=batchmode -jobname '\image' '\texsource';
    dvips -o '\image'.ps '\image.dvi'}}
\fi
%    \end{macrocode}
% Provide the \cmd{\inputtikz} command:
%    \begin{macrocode}
\providecommand{\inputtikz}[1]{%
\tikzset{/tikz/external/export=true}%
\tikzsetnextfilename{\tikzmake@pictprefix#1}%
\tikzpicturedependsonfile{\tikzmake@pictdir/\tikzmake@pictprefix#1.tex}%
\tikzsetnextfilename{\tikzmake@pictprefix#1}%
\input{\tikzmake@pictdir/\tikzmake@pictprefix#1.tex}%
\tikzset{/tikz/external/export=false}}
%    \end{macrocode}
% Provide the \cmd{\inputplot} command:
%    \begin{macrocode}
\providecommand{\inputplot}[1]{%
\tikzset{/tikz/external/export=true}%
\tikzsetnextfilename{\tikzmake@plotprefix#1}%
\tikzpicturedependsonfile{\tikzmake@plotdir/\tikzmake@plotprefix#1.tex
  \tikzmake@datadir/\tikzmake@dataprefix#1.\tikzmake@dataext}%
\tikzsetnextfilename{\tikzmake@plotprefix#1}%
\tikzifexternalizing{%
  \pgfplotstableread{\tikzmake@datadir/\tikzmake@dataprefix#1.\tikzmake@dataext}%
  \plotdata}{}
\input{\tikzmake@plotdir/\tikzmake@plotprefix#1.tex}%
\tikzset{/tikz/external/export=false}}
%    \end{macrocode}
% \Finale
\endinput
# Makefile for the tikzmake LaTeX package

mainsource := $(shell grep -l '^[^%]*\\documentclass' *.dtx)
package = $(mainsource:.dtx=)
test = test-tikzmake

packfiles = ${package}.dtx ${package}.ins ${package}.pdf Makefile tikzmake ${test}.pdf README.txt
tempfiles = *.aux *.log *.glo *.ind *.idx *.out *.svn *.toc *.ilg *.gls *.hd
generated = ${package}.pdf ${package}.sty ${package}.tar.gz

.PHONY: all package doc test new clean clean_all

all: package doc test clean tar.gz message

new: clean_all all

doc: ${package}.pdf

package: ${package}.sty

test: ${test}.pdf

${package}.pdf: ${package}.dtx ${package}.sty
	pdflatex ${package}.dtx
	pdflatex ${package}.dtx
	pdflatex ${package}.dtx
	makeindex -s gind.ist -o ${package}.ind ${package}.idx
	makeindex -s gglo.ist -o ${package}.gls ${package}.glo
	pdflatex ${package}.dtx
	pdflatex ${package}.dtx

${package}.sty: ${package}.dtx ${package}.ins
	-@rm -f ${package}.sty
	latex ${package}.ins

${test}.pdf: ${test}.tex
	make -f tikzmake

clean:
	-@rm -f ${tempfiles}
	make -f tikzmake clean

clean_all:
	-@rm -f ${tempfiles} ${generated}
	make -f tikzmake clean_all

tar.gz: package doc test clean ${package}.tar.gz

${package}.tar.gz:
	tar -czf $@ ${packfiles}

message:
	@echo
	@echo
	@echo "*************************************************************"
	@echo "*                          DONE"
	@echo "*************************************************************"
	@echo "* You might want to move the newly created $(package).sty and"
	@echo "* $(package).pdf to your texmf tree, e.g:"
	@echo "*    ~/texmf/tex/latex/$(package)/$(package).sty"
	@echo "* and"
	@echo "*    ~/texmf/doc/$(package)/$(package).pdf"
	@echo "* respectively."
	@echo "*************************************************************"
	@echo
